# nginx ingress Kubes

> Kubernetes nginx deployment with LetsEncrypt deployment

## Setting up
### Helm Tiller
```bash
# set up roles
kubectl --namespace kube-system create serviceaccount tiller

kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

kubectl --namespace kube-system patch deploy tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}' 

# install the tiller pod
helm init --tiller-namespace tiller-world --upgrade
```

### Certmanager
```bash
kubectl create namespace cert-manager
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v0.10.1/cert-manager.yaml
```

### Ingress
```bash
kubectl create -f https://github.com/kubernetes/ingress-nginx/raw/master/deploy/static/mandatory.yaml

helm install stable/nginx-ingress --name nginx-ingress --set controller.publishService.enabled=true
```

### apply the things
```bash
kubectl create -f FILENAME
```